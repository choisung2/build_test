import React from 'react';
import styled from 'styled-components';

export const Footer = () => {
  const FooterCont = styled.footer`
    position: relative;
    margin-top: 100px;
    border-top: 1px solid #ddd;
    .a {
      display: flex;
    }
    section {
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: space-between;
      padding: 24px 0;
      .project-info {
        font-size: 0.8rem;

        text-align: end;
        .copyright {
          font-style: italic;
          margin-bottom: 10px;
          @media (max-width: 640px) {
            margin: unset;
          }
        }
      }
    }
  `;

  const Credit = styled.ul`
    display: flex;

    li > small {
      font-size: 0.8rem;
    }
    li {
      .madeby {
        margin-right: 8px;
        font-style: italic;
      }
    }
  `;

  const FooterLogo = styled.div`
    img {
      height: 30px;
      width: 73px;
      vertical-align: -10px;
    }
    h1 {
      font-size: 12px;
      font-weight: 400;
    }
  `;

  const Top = styled.a`
    display: flex;
    align-items: center;
    justify-content: center;
    position: absolute;
    top: -28px;
    right: 28px;
    width: 48px;
    height: 48px;

    border-radius: 50%;

    font-size: 10px;
    overflow: hidden;

    &::before,
    &::after {
      content: '';
      display: block;
      left: 50%;
      position: absolute;
      transform: translate(-50%, -50%) rotate(135deg);
      transition: all 0.2s;
    }
    &::before {
      background: #fff;
      width: 100%;
      height: 100%;
      border-radius: 50%;
      top: 50%;
    }
    &::after {
      border-width: 0 0 2px 2px;
      width: 10px;
      height: 10px;
      top: 55%;
    }

    &:hover {
    }
  `;

  return (
    <FooterCont>
      <section className="max-width">
        <section className="a">
          <h1>(주)티맥스커머스</h1>
          <span>기술지원센터: 경기도 성남시 분당구 황새울로258번길 31</span>
          <span>R&D센터: 경기도 성남시 분당구 황새울로258번길 31</span>
          <span>대표이사: 곽태우 | 사업자 번호: 523-87-02574 | 통신판매업</span>
          <span>본 사이트의 모든 콘텐츠는 저작권법의 보호를 바는 바, 무단 전재, 복사, 배포 등을 금합니다.</span>
        </section>
        <div className="project-info">
          {/* <p className="copyright">&copy; 2022 - {year !== 2022 && year} MOSS. All rights Reserved.</p> */}
          <Credit>
            <small>Copyright ⒞ 2022. TmaxCommerce, All Rights Reserved.</small>
          </Credit>
        </div>
      </section>
    </FooterCont>
  );
};
