import React from 'react';
import { Footer } from './Footer';
import { Title } from './Title';

export const Layout = (props: { children: React.ReactNode }) => {
  return (
    <>
      <Title title="티맥스커머스 홈페이지" />
      <section className="max-width">{props.children}</section>
      <Footer />
    </>
  );
};
