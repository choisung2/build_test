interface TitleProps {
  title: string;
}

export const Title = ({ title }: TitleProps) => {
  return (
    <section>
      <title>{title} | MOSS</title>

      <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />

      <meta name="Description" content="" />
      <link rel="icon" type="image/x-icon" href="#" />
      <meta property="og:title" content="commercHomepage" />
      <meta property="og:type" content="website" />
      <meta property="og:site_name" content="commercHomepage" />
      <meta property="og:description" content="" />
      <meta property="og:image" content="#" />
    </section>
  );
};
